$(document).ready(function(){
  
  var tabWrapper = $('#tab-block'),
      tabMenu = tabWrapper.find('.tab-menu  li'),
      tabContent = tabWrapper.find('.tab-cont > .tab-pane');
  
  tabContent.not(':first-child').hide();
  
  tabMenu.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  
  tabMenu.click(function(){
    var tabData = $(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show(); 
  });
  
  $('.tab-menu > li').click(function(){
    var before = $('.tab-menu li.active');
    before.removeClass('active');
    $(this).addClass('active');
   });
  
});