$(document).on('ready', function() {
  $(".home-slider-tm").slick({
    lazyLoad: 'ondemand', // ondemand progressive anticipated
    autoplay: true,
    arrows: false,
    dots: true,
    infinite: true
  });
});